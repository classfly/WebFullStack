package com.mytaobao.enums;

/**
 * OrderStateEnum 订单状态枚举值
 *
 * @author classfly
 * @version 2024/11/12 05:32
 **/
public enum OrderStateEnum {
    kCreated("已创建（初始状态）", 1),
    kPaying("支付中", 2),
    kPaySuccess("支付成功", 3),
    kPayFailed("支付失败", 4),
    kRefunding("退款中", 5),
    kRefunded("已退款", 6),
    kDeleted("已删除", 7);

    String message;

    int value;

    OrderStateEnum(String message, int value) {
        this.message = message;
        this.value = value;
    }

    public String message() {
        return this.message;
    }

    public int value() {
        return this.value;
    }
}