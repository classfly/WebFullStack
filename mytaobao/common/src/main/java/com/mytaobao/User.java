package com.mytaobao;

import java.io.Serializable;

public class User implements Serializable {
    /**
     * 用户账号
     */
    long uaccount;

    /**
     * 用户密码
     */
    String upassword;

    /**
     * 用户姓名
     */
    String uname;

    /**
     * 用户性别
     */
    String usex;

    public long getUaccount() {
        return uaccount;
    }

    public void setUaccount(long uaccount) {
        this.uaccount = uaccount;
    }

    public String getUpassword() {
        return upassword;
    }

    public void setUpassword(String upassword) {
        this.upassword = upassword;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUsex() {
        return usex;
    }

    public void setUsex(String usex) {
        this.usex = usex;
    }
}
