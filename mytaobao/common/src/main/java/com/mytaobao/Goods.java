package com.mytaobao;

import java.io.Serializable;

public class Goods implements Serializable {
    long gid;

    String gname;

    String gremain;

    String gdetails;

    long gprice;

    int gtypes;

    public long getGid() {
        return gid;
    }

    public void setGid(long gid) {
        this.gid = gid;
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname;
    }

    public String getGremain() {
        return gremain;
    }

    public void setGremain(String gremain) {
        this.gremain = gremain;
    }

    public String getGdetails() {
        return gdetails;
    }

    public void setGdetails(String gdetails) {
        this.gdetails = gdetails;
    }

    public long getGprice() {
        return gprice;
    }

    public void setGprice(long gprice) {
        this.gprice = gprice;
    }

    public int getGtypes() {
        return gtypes;
    }

    public void setGtypes(int gtypes) {
        this.gtypes = gtypes;
    }

}
