package com.xpwu.secondary.config;

/**
 * @author caoxue
 */
public class AlipayConfig {
	/** 商户appid */
	public static String APPID = "2019091867566349";
	/**
	 * 私钥 pkcs8格式的
	 */
	public static String RSA_PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCiX+F6DPDsuJ+2agIFeHdZSLY93DEnymmxULGRJLNGvLptX27ZhqRsGNmNzJrYjXSd/s4jOdBztozZdRSV6g8fn0tNywG14i6xHb7NRRgG1fLUfH39InEuNK0jvB/c3NmuztjEldZ9gB++iy8j4RxP/UKinRsdd4v9yzTspJer49uUgvRkycmo83gkCFa0zXI1I6Ciu0flwrsC+81W17aoMhlzeKJ5DRcJGqlOEKNIXA9R2LlZ2i+sr5NiDdVAwht0/CRhelMnBllGX2zqf7yob2dmm6Y+JKNSoQKVGxfxLrgp7D+bSmIVOmCSG29/lU3f8efSDZZFFSZimBo+zodPAgMBAAECggEBAJIdMatAxfRe1umxTwxZMRzY/5V+9YTW8nErKf5wRhsbba9KN1frfz3+xpaBTb4c3JnoVtsABYJ6/RgZ8WJN8zoDgCIK0gk39chP+giBanOMRy3KsRz+x1ZK9Jq5iPqYihJx9EJCi3b3upkealchFPct8a6fLW1AzGqfV4qzOgDkYYYcB8ap3kRA+R/xanTKuTTMBq3AetzmBQgDGjVLI6lRpa/PyvIU0aRhDyBR+xPHsFPPBkfOxye4Kos/rtUrv+JSwsYDtxznwyCXDucrWkfgMpZshBhA6WX5SPaPOW985O9UOWY5H3vRWcrqFcorYlVSWPbUZx2pQ7Nls1ON5cECgYEA6nw2eQ2gYEmE5a0RcSIr046fCVYUamVMoJwyDcc9aTICJWeHyqVKHgiCX7cHqEma41SGr77rkf8H0lyNtbQMh/SAVS0UEGKlPZCW+brN+ylCejO/MNBExUbICbm42E06Hyorv9aLeXO0hkFOC7sYNOMISE9RjIfXKMMOCkPe878CgYEAsUXfFUHHsapgXxT2R1Qvz2RiJNkrlv2xPmnX7vHHfu/tKPA/hdqw0fsspOG4QEbhLCXidXApNr6u6BnMKq0OG8eX6FgCklMI5HUnqlUB8XnIMsoKhaS4v6xsShNoFTEsC+Jb6tkKn7eY13DY6oQ6yK2lFnjf7/9X+mQFAIwXEHECgYAZXSBDTd8IK0ut0F+vniOhea7oQqBotdZpFRww81lINLmYtC8zE1gGEaOQmOrCIFwZtP3wqrUukKiW754Ahp5IYoEzNXITJ6h6FVbPYo5/WYIwV8KZlB2sPTDMJIPDGe9siZI76QgjNgnT7+JyUGWY6untcG8AxXzXkunE4HyQXwKBgGjH5Ok6nV9dVmM9D6jvkObj7cLb/pDb2PUMOM8KaCGWjTaictmafuNOtH+sz2xmuNeQeP9zUVkDcnzMKsqayXkQC7EQvExi+iGXQd/DKUfi9LtYgsmlqHHa55MNT3sfOmq33B6/NTFcouHM2NodWGWfhbH3tl20LvtYPUCqjUNxAoGBAMlFpXr60ImIY/UxvrqvxdrrtPMF5zNtWRHFAFnzmowwQW32PhrFHJq7MaB7ptKuoUnf2zf5EiSfii7uiOjhNXPQsTDcjG9t2yu4at4xjmJknVkcgyfrrktVR8uG6DK0H9GpBp0w1Io16uWgRym8SljO9bQgbciqVAobmnX3VMpR";
	/**
	 * 服务器异步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	 */
	public static String NOTIFY_URL = "http://49.232.24.206:9001/secondary/alipayNotify";
	/**
	 * 页面跳转同步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问 商户可以自定义同步跳转地址
 	 */
	public static String RETURN_URL = "http://msecondary.com/";
	/**
	 * 请求网关地址
 	 */
	public static String URL = "https://openapi.alipay.com/gateway.do";
	//	public static String URL = "https://openapi.alipaydev.com/gateway.do"

	/** 编码 */
	public static String CHARSET = "UTF-8";
	/** 返回格式 */
	public static String FORMAT = "json";
	/** 支付宝公钥 */
	public static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjTUIK45jDaO4xax8ZOPtfBiRz4Rwv78OFfkgwgMZq4FhPNEwBHmijIdjaKcxGl/cE2IIsm3huqdb7howj95P/nUorTew6VFxLCkxTW3f5PYuvzp44qeKvirBQkrccLIqpdnQ5M7kMdmI9dXc3HPG4VO+ZD7Zi73gXEzGlkbioz5fwzVpGfAFjvCzb49s1kxgEeIwAFLw6isiNJWAddzxagabbs6tVVM8XD/T9WmXyK00cASvqNd0TCeDO73ssW4hBch671MQkp5nbb8ZNuzZhU5/mkKuVjZk6jtqoABXhkfj6J5TNu6krNxDoPPK3nBkX+yOt1LMEFNQC8od0OTUrwIDAQAB";
//	public static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjrEVFMOSiNJXaRNKicQuQdsREraftDA9Tua3WNZwcpeXeh8Wrt+V9JilLqSa7N7sVqwpvv8zWChgXhX/A96hEg97Oxe6GKUmzaZRNh0cZZ88vpkn5tlgL4mH/dhSr3Ip00kvM4rHq9PwuT4k7z1DpZAf1eghK8Q5BgxL88d0X07m9X96Ijd0yMkXArzD7jg+noqfbztEKoH3kPMRJC2w4ByVdweWUT2PwrlATpZZtYLmtDvUKG/sOkNAIKEMg3Rut1oKWpjyYanzDgS7Cg3awr1KPTl9rHCazk15aNYowmYtVabKwbGVToCAGK+qQ1gT3ELhkGnf3+h53fukNqRH+wIDAQAB";
	/** 日志记录目录 */
	public static String LOG_PATH = "/log";
	/** RSA2 */
	public static String SIGNTYPE = "RSA2";
}
