package com.xpwu.secondary.service;

import com.xpwu.secondary.bo.AddressBO;
import com.xpwu.secondary.bo.BaseBO;
import com.xpwu.secondary.entity.Address;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: caoxue
 * @date: 2019/10/15 19:47
 * @description:
 * @version: 1.0
 */
public interface AddressService extends BaseService<Address, Integer> {
    /**
     * 获取地址列表
     * @param bo
     * @return
     */
    List<Address> getAddressList(BaseBO bo);

    /**
     * 保存地址
     * @param bo
     */
    void saveAddress(AddressBO bo);

    /**
     * 修改收货地址
     * @param bo
     */
    void updateAddress(AddressBO bo);

    /**
     * 删除收货地址
     * @param bo
     */
    void delAddress(AddressBO bo);
}
