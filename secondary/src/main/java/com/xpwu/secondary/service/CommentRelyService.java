package com.xpwu.secondary.service;

import com.xpwu.secondary.bo.CommentReplyBO;
import com.xpwu.secondary.entity.CommentReply;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: caoxue
 * @date: 2019/8/17 13:28
 * @description: 评论/回复接口
 * @version: 1.0
 */
public interface CommentRelyService extends BaseService<CommentReply, Integer> {

    /**
     * 评论/回复
     * @param bo
     */
    void commentOrReply(CommentReplyBO bo);

}
