package com.xpwu.secondary.service.impl;

import com.xpwu.secondary.bo.CommonOrderBO;
import com.xpwu.secondary.bo.OrderBO;
import com.xpwu.secondary.constants.RedisConstants;
import com.xpwu.secondary.entity.Address;
import com.xpwu.secondary.entity.Order;
import com.xpwu.secondary.entity.User;
import com.xpwu.secondary.enums.RabbitMqEnum;
import com.xpwu.secondary.mapper.AddressMapper;
import com.xpwu.secondary.mapper.OrderMapper;
import com.xpwu.secondary.mapper.ProductMapper;
import com.xpwu.secondary.rabbitmq.producer.DirectMqSender;
import com.xpwu.secondary.service.OrderService;
import com.xpwu.secondary.service.UserService;
import com.xpwu.secondary.utils.Assertion;
import com.xpwu.secondary.utils.Detect;
import com.xpwu.secondary.utils.RedisUtils;
import com.xpwu.secondary.vo.OrderVO;
import com.xpwu.secondary.vo.ProductVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: caoxue
 * @date: 2019/9/15 15:23
 * @description:
 * @version: 1.0
 */
@Service
@Slf4j
public class OrderServiceImpl extends BaseServiceImpl<Order, Integer> implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private DirectMqSender directMqSender;

    @Autowired
    private AddressMapper addressMapper;

    @Value("${spring.rabbitmq.order.ttl}")
    private int ttl;

    @Override
    public OrderVO placeOrder(OrderBO bo) {
        // 校验token
        User user = userService.checkToken(bo.getToken());
        // 校验商品是否存在
        ProductVO productVO = productMapper.selectProductInfoAndTradeStatus(bo.getProductId());
        Assertion.notNull(productVO, "商品信息不存在");
        Assertion.isTrue(2 == productVO.getTradeStatus(), "该商品正在交易中或已卖出");
        Address address = addressMapper.selectByPrimaryKey(bo.getAddressId());
        Assertion.notNull(address, "收货信息不存在");
        Assertion.isTrue(1 == address.getStatus(), "收货信息已被删除");
        Date date = new Date();
        // 生成待支付订单
        Order order = new Order();
        order.setBuyingUserId(user.getUserId());
        order.setCreateTime(date);
        order.setOrderAmount(productVO.getProductPrice());
        order.setProductId(productVO.getId());
        order.setUpdateTime(date);
        String consigneeAddress = address.getProvince() + " " + address.getCity() + address.getDistrict() + address.getStreet()
                + " " + address.getAddressDetail();
        order.setConsigneeAddress(consigneeAddress);
        order.setConsigneeMobile(address.getConsigneeMobile());
        order.setConsigneeName(address.getConsigneeName());
        orderMapper.insertSelective(order);
        int orderId = order.getOrderId();
        // 发送延时队列
        directMqSender.sendTtlMessage(RabbitMqEnum.RoutingEnum.SECONDARY_ORDER_DEAD_LETTER_ROUTING, orderId + "");
        // 订单有效时长写入redis
        String key = String.format(RedisConstants.ORDER_ID, orderId);
        RedisUtils.setEx(key, ttl / 1000, orderId + "");
        // 组装返回信息
        OrderVO vo = new OrderVO();
        vo.setOrderId(orderId);
        vo.setCreateTime(date);
        vo.setOrderAmount(order.getOrderAmount());
        vo.setPayStatus(1);
        vo.setTradeStatus(1);
        vo.setProductDesc(productVO.getProductDesc());
        vo.setProductImgs(productVO.getProductImgs());
        vo.setBuyerUserName(user.getUserName());
        vo.setExpiredTime((long) ttl);
        return vo;
    }

    @Override
    public void cancelOrder(CommonOrderBO bo) {
        // 校验token
        User user = userService.checkToken(bo.getToken());
        // 校验订单是否存在
        Order order = orderMapper.selectByPrimaryKey(bo.getOrderId());
        Assertion.notNull(order, "订单不存在");
        Assertion.isTrue(1 == order.getTradeStatus(), "订单已取消或已结算，请勿重复操作");
        Assertion.isTrue(1 == order.getPayStatus() || 4 == order.getPayStatus(), "订单正在付款中或已付款，无法取消");
        Order update = new Order();
        update.setOrderId(bo.getOrderId());
        update.setTradeStatus(2);
        update.setUpdateTime(new Date());
        orderMapper.updateByPrimaryKeySelective(update);
    }

    @Override
    public OrderVO getOrderDetail(CommonOrderBO bo) {
        // 校验token
        User user = userService.checkToken(bo.getToken());
        OrderVO vo = orderMapper.selectOrderDetailByOrderId(bo.getOrderId());
        Assertion.notNull(vo, "订单信息不存在");
        String key = String.format(RedisConstants.ORDER_ID, bo.getOrderId());
        long expiredTime = RedisUtils.pttl(key);
        log.info("expiredTime:" + expiredTime);
        vo.setExpiredTime(Detect.isPositive(expiredTime) ? expiredTime / 1000 : 0);
        return vo;
    }
}
