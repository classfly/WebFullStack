package com.xpwu.secondary.service;

import com.xpwu.secondary.bo.CommonOrderBO;
import com.xpwu.secondary.bo.OrderBO;
import com.xpwu.secondary.entity.Order;
import com.xpwu.secondary.vo.OrderVO;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: caoxue
 * @date: 2019/9/15 15:22
 * @description: 订单相关接口
 * @version: 1.0
 */
public interface OrderService extends BaseService<Order, Integer> {

    /**
     * 下单--购买商品
     * @param bo
     * @return
     */
    @Transactional(rollbackFor = Exception.class, isolation = Isolation.READ_COMMITTED)
    OrderVO placeOrder(OrderBO bo);

    /**
     * 取消订单
     * @param bo
     */
    void cancelOrder(CommonOrderBO bo);

    /**
     * 订单详情信息
     * @param bo
     * @return
     */
    OrderVO getOrderDetail(CommonOrderBO bo);
}
