package com.xpwu.secondary.service.impl;

import com.xpwu.secondary.entity.PaymentNotifyLog;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: caoxue
 * @date: 2019/9/18 10:56
 * @description:
 * @version: 1.0
 */
@Service
public class PaymentNotifyLogServiceImpl extends BaseServiceImpl<PaymentNotifyLog, Integer> {
}
