package com.xpwu.secondary.service.impl;

import com.xpwu.secondary.entity.Banner;
import com.xpwu.secondary.service.BannerService;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: caoxue
 * @date: 2019/8/8 17:01
 * @description:
 * @version: 1.0
 */
@Service
public class BannerServiceImpl extends BaseServiceImpl<Banner, Integer> implements BannerService {
}
