package com.xpwu.secondary.service;

import com.xpwu.secondary.entity.PaymentNotifyLog;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: caoxue
 * @date: 2019/9/18 10:55
 * @description:
 * @version: 1.0
 */
public interface PaymentNotifyLogService extends BaseService<PaymentNotifyLog, Integer> {
}
