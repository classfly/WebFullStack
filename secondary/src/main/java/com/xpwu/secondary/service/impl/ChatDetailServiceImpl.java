package com.xpwu.secondary.service.impl;

import com.xpwu.secondary.entity.Chat;
import com.xpwu.secondary.service.ChatDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: caoxue
 * @date: 2019/8/19 17:53
 * @description:
 * @version: 1.0
 */
@Service
@Slf4j
public class ChatDetailServiceImpl extends BaseServiceImpl<Chat, Integer> implements ChatDetailService {
}
