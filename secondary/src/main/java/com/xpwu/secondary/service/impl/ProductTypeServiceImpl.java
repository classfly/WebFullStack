package com.xpwu.secondary.service.impl;

import com.xpwu.secondary.entity.ProductType;
import com.xpwu.secondary.service.ProductTypeService;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: caoxue
 * @date: 2019/8/8 17:03
 * @description:
 * @version: 1.0
 */
@Service
public class ProductTypeServiceImpl extends BaseServiceImpl<ProductType, Integer> implements ProductTypeService {
}
