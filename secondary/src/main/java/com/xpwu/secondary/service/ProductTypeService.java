package com.xpwu.secondary.service;

import com.xpwu.secondary.entity.ProductType;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: caoxue
 * @date: 2019/8/8 17:02
 * @description:
 * @version: 1.0
 */
public interface ProductTypeService extends BaseService<ProductType, Integer> {
}
