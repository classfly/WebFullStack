package com.xpwu.secondary.service;

import com.xpwu.secondary.bo.PraiseBO;
import com.xpwu.secondary.entity.Praise;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: caoxue
 * @date: 2019/8/15 15:57
 * @description: 点赞相关接口
 * @version: 1.0
 */
public interface PraiseService extends BaseService<Praise, Integer> {

    /**
     * 点赞/取消点赞
     * @param bo
     */
    void praiseOrUnPraise(PraiseBO bo);

    /**
     * 获取点赞列表
     * @param bo
     * @return
     */
    Map<String, Object> getPraiseList(PraiseBO bo);

}
