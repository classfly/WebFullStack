package com.xpwu.secondary.service;

import com.xpwu.secondary.entity.Banner;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: caoxue
 * @date: 2019/8/8 17:01
 * @description:
 * @version: 1.0
 */
public interface BannerService extends BaseService<Banner, Integer> {
}
