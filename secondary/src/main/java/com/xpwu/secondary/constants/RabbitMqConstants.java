package com.xpwu.secondary.constants;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: caoxue
 * @date: 2019/10/15 9:27
 * @description: rabbitMq常量
 * @version: 1.0
 */
public class RabbitMqConstants {

    /**
     * 死信队列交换机
     */
    public final static String DEAD_LETTER_EXCHANGE = "x-dead-letter-exchange";

    /**
     * 死信队列路由key
     */
    public final static String DEAD_LETTER_ROUTING_KEY = "x-dead-letter-routing-key";
}
