package com.xpwu.secondary.controller;

import com.xpwu.secondary.service.BannerService;
import com.xpwu.secondary.service.ProductService;
import com.xpwu.secondary.vo.ResponseVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: caoxue
 * @date: 2019/8/8 17:08
 * @description: banner轮播图
 * @version: 1.0
 */
@RestController
@Slf4j
public class BannerController extends BaseController {

    @Autowired
    private BannerService bannerService;

    @Autowired
    private ProductService productService;

    /**
     * 获取首页轮播图列表
     * @return
     */
    @RequestMapping(value = "getBannerList", method = RequestMethod.GET)
    public ResponseVO getBannerList() {
        return ResponseVO.success(productService.getBannerList());
    }
}
