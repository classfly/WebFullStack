package com.xpwu.secondary.controller;

import com.xpwu.secondary.common.Header;
import com.xpwu.secondary.enums.CodeEnum;
import com.xpwu.secondary.exception.BusinessException;
import com.xpwu.secondary.utils.Assertion;
import com.xpwu.secondary.utils.Detect;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: caoxue
 * @date: 2019/8/10 0:01
 * @description: 基础服务controller
 * @version: 1.0
 */
@Slf4j
public class BaseController {

    @Autowired
    private HttpServletRequest request;

    /**
     * 获取头部信息
     * @return
     */
    public Header getHeader() {
        String requestId = request.getHeader("requestId");
        String timestamp = request.getHeader("timestamp");
        String deviceId = request.getHeader("deviceId");
        String clientVersion = request.getHeader("clientVersion");
        String token = request.getHeader("token");
        Header header = new Header(requestId, deviceId, clientVersion, Detect.notEmpty(timestamp) ? Long.valueOf(timestamp) : 0L, token);
        log.info("header信息：" + header.toString());
        return header;
    }

    /**
     * 校验token是否为空
     * @return
     */
    public String checkToken() {
        Header header = getHeader();
        Assertion.notNull(header, "请求头header不能为空");
        String token = header.getToken();
        if (!Detect.notEmpty(token)) {
            throw new BusinessException(CodeEnum.TOKEN_FAILURE);
        }
        return token;
    }

    /**
     * 获取token
     * @return
     */
    public String getToken() {
        Header header = getHeader();
        return header.getToken();
    }
}
