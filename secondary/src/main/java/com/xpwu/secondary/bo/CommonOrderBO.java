package com.xpwu.secondary.bo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: caoxue
 * @date: 2019/10/14 8:46
 * @description:
 * @version: 1.0
 */
@Setter
@Getter
@ToString(callSuper = true)
public class CommonOrderBO extends BaseBO {

    private static final long serialVersionUID = -9078025283345783996L;
    /**
     * 订单编号
     */
    private Integer orderId;

}
