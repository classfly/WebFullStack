package com.xpwu.secondary.bo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: caoxue
 * @date: 2019/8/24 23:35
 * @description: 查询聊天详情入参
 * @version: 1.0
 */
@Setter
@Getter
@ToString(callSuper = true)
public class ChatDetailBO extends BaseBO {

    private static final long serialVersionUID = 8732608165375370704L;

    /**
     * 聊天编号
     */
    private String chatId;

}
