package com.xpwu.secondary;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * @author caoxue
 * @date 2019/8/6 9:34
 */
@SpringBootApplication
@MapperScan(basePackages = "com.xpwu.secondary.mapper")
@Slf4j
public class SecondaryApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecondaryApplication.class, args);

        CountDownLatch countDownLatch = new CountDownLatch(1);

        // 心跳检测程序是否存活
        try {
            while (true) {
                if (!countDownLatch.await(60, TimeUnit.SECONDS)) {
                    log.info("----------------- SecondaryApplication server id alive---------------------");
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
