package com.xpwu.secondary.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: caoxue
 * @date: 2019/8/14 20:10
 * @description: token信息
 * @version: 1.0
 */
@Setter
@Getter
@ToString(callSuper = true)
public class TokenVO implements Serializable {

    private static final long serialVersionUID = 1894308742642787496L;
    /**
     * 客户编号
     */
    private String userId;

    /**
     * 手机号
     */
    private String mobile;

}
