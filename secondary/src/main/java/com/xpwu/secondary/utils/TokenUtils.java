package com.xpwu.secondary.utils;

import java.util.Date;
import java.util.Objects;

/**
 * @description: Token工具类
 * @author: caoxue
 * @create: 2019/8/10
 **/
public class TokenUtils {

    /**
     * 根据用户id及当前时间生成token
     *
     * @param userId
     * @return
     */
    public static String getToken(String userId) {
        String md5Str = userId + DateUtils.getDate(new Date());
        return Objects.requireNonNull(Md5Utils.stringToMd5(md5Str)).toUpperCase();
    }

}
