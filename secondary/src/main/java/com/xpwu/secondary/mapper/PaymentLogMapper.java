package com.xpwu.secondary.mapper;

import com.xpwu.secondary.entity.PaymentLog;
import tk.mybatis.mapper.common.Mapper;

public interface PaymentLogMapper extends Mapper<PaymentLog> {
}