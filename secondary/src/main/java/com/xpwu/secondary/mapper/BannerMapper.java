package com.xpwu.secondary.mapper;

import com.xpwu.secondary.entity.Banner;
import tk.mybatis.mapper.common.Mapper;

public interface BannerMapper extends Mapper<Banner> {
}
