package com.xpwu.secondary.mapper;

import com.xpwu.secondary.entity.RequestLog;
import tk.mybatis.mapper.common.Mapper;

public interface RequestLogMapper extends Mapper<RequestLog> {
}