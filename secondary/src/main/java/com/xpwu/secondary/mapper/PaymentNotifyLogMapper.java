package com.xpwu.secondary.mapper;

import com.xpwu.secondary.entity.PaymentNotifyLog;
import tk.mybatis.mapper.common.Mapper;

public interface PaymentNotifyLogMapper extends Mapper<PaymentNotifyLog> {
}