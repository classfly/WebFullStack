package com.xpwu.secondary.mapper;

import com.xpwu.secondary.entity.ChatList;
import com.xpwu.secondary.vo.ChatVO;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface ChatListMapper extends Mapper<ChatList> {
    /**
     * 查询聊天列表
     * @param userId
     * @return
     */
    List<ChatVO> selectChatList(String userId);
}
