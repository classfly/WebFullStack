package com.xpwu.secondary.mapper;

import com.xpwu.secondary.entity.Chat;
import tk.mybatis.mapper.common.Mapper;

public interface ChatMapper extends Mapper<Chat> {
}