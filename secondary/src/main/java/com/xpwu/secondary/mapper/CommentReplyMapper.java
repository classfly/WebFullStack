package com.xpwu.secondary.mapper;

import com.xpwu.secondary.entity.CommentReply;
import tk.mybatis.mapper.common.Mapper;

public interface CommentReplyMapper extends Mapper<CommentReply> {
}