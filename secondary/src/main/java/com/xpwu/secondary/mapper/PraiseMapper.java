package com.xpwu.secondary.mapper;

import com.xpwu.secondary.entity.Praise;
import tk.mybatis.mapper.common.Mapper;

public interface PraiseMapper extends Mapper<Praise> {
}
