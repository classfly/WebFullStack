package com.xpwu.secondary.mapper;

import com.xpwu.secondary.entity.ProductType;
import tk.mybatis.mapper.common.Mapper;

public interface ProductTypeMapper extends Mapper<ProductType> {
}
