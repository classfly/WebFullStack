package com.xpwu.secondary.mapper;

import com.xpwu.secondary.entity.ChatDetail;
import tk.mybatis.mapper.common.Mapper;

public interface ChatDetailMapper extends Mapper<ChatDetail> {
}