package com.xpwu.secondary.mapper;

import com.xpwu.secondary.entity.Address;
import tk.mybatis.mapper.common.Mapper;

public interface AddressMapper extends Mapper<Address> {
}