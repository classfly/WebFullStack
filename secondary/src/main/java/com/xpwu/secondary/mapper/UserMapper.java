package com.xpwu.secondary.mapper;

import com.xpwu.secondary.entity.User;
import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {
}