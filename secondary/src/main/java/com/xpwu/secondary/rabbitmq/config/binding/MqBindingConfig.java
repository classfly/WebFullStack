package com.xpwu.secondary.rabbitmq.config.binding;

import com.xpwu.secondary.enums.RabbitMqEnum;
import com.xpwu.secondary.rabbitmq.config.RabbitMqConfig;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 用于配置交换机和队列对应关系
 * 新增消息队列应该按照如下步骤
 * 1、增加queue bean，参见queueXXXX方法
 * 2、增加queue和exchange的binding
 *
 * @author caoxue
 **/
@Configuration
@Slf4j
public class MqBindingConfig extends RabbitMqConfig {

    private static final Logger logger = LoggerFactory.getLogger(MqBindingConfig.class);


    @Bean
    Binding bindingOrderQueue(@Qualifier("orderQueue") Queue orderQueue,
                              @Qualifier("orderDeadLetterExchange") DirectExchange orderDeadLetterExchange,
                              RabbitAdmin rabbitAdmin){
        Binding binding = BindingBuilder.bind(orderQueue)
                        .to(orderDeadLetterExchange).with(RabbitMqEnum
                        .RoutingEnum.SECONDARY_ORDER_ROUTING.getCode());
        rabbitAdmin.declareBinding(binding);
        log.debug("订单队列绑定orderDeadLetterExchange交换机成功");
        return binding;
    }

    @Bean
    public Binding bindingOrderDeadLetterQueue(@Qualifier("orderDeadLetterQueue") Queue orderDeadLetterQueue,
                                          @Qualifier("orderDeadLetterExchange") DirectExchange orderDeadLetterExchange,
                                          RabbitAdmin rabbitAdmin) {
        Binding binding = BindingBuilder.bind(orderDeadLetterQueue)
                .to(orderDeadLetterExchange).with(RabbitMqEnum
                        .RoutingEnum.SECONDARY_ORDER_DEAD_LETTER_ROUTING.getCode());
        rabbitAdmin.declareBinding(binding);
        log.debug("订单死信队列绑定orderDeadLetterExchange交换机成功");
        return binding;
    }
}
