package com.xpwu.secondary.rabbitmq.config.exchange;


import com.xpwu.secondary.enums.RabbitMqEnum;
import com.xpwu.secondary.rabbitmq.config.RabbitMqConfig;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 用于配置交换机和队列对应关系
 * 新增消息队列应该按照如下步骤
 * 1、增加queue bean，参见queueXXXX方法
 * 2、增加queue和exchange的binding
 *
 * @author caoxue
 **/
@Configuration
@Slf4j
public class MqExchangeConfig extends RabbitMqConfig {
    private static final Logger logger = LoggerFactory.getLogger(MqExchangeConfig.class);

    /**
     * 直连型交换机
     */
    @Bean("contractDirectExchange")
    DirectExchange contractDirectExchange(RabbitAdmin rabbitAdmin) {
        DirectExchange contractDirectExchange = new DirectExchange(RabbitMqEnum.Exchange.SECONDARY_CONTRACT_DIRECT.getCode());
        rabbitAdmin.declareExchange(contractDirectExchange);
        log.info("主题型交换机-点对点初始化成功!");
        return contractDirectExchange;
    }

    @Bean("orderDeadLetterExchange")
    DirectExchange orderDeadLetterExchange(RabbitAdmin rabbitAdmin) {
        DirectExchange orderDeadLetterExchange = new DirectExchange(RabbitMqEnum.Exchange.SECONDARY_ORDER_DEAD_LETTER_ECHANGE.getCode());
        rabbitAdmin.declareExchange(orderDeadLetterExchange);
        log.info("死信交换机-点对点初始化成功!");
        return orderDeadLetterExchange;
    }
}
