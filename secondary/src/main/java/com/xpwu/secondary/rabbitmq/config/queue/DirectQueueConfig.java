package com.xpwu.secondary.rabbitmq.config.queue;

import com.xpwu.secondary.constants.RabbitMqConstants;
import com.xpwu.secondary.enums.RabbitMqEnum;
import com.xpwu.secondary.rabbitmq.config.RabbitMqConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * 增加新的队列
 * @author caoxue
 */
@Configuration
@Slf4j
public class DirectQueueConfig extends RabbitMqConfig {
    /**
     * 订单死信队列
     * @param rabbitAdmin
     * @return
     */
    @Bean("orderDeadLetterQueue")
    Queue orderDeadLetterQueue(RabbitAdmin rabbitAdmin){
        Map<String, Object> args = new HashMap<>(16);
        args.put(RabbitMqConstants.DEAD_LETTER_EXCHANGE, RabbitMqEnum.Exchange.SECONDARY_ORDER_DEAD_LETTER_ECHANGE.getCode());
        args.put(RabbitMqConstants.DEAD_LETTER_ROUTING_KEY, RabbitMqEnum.RoutingEnum.SECONDARY_ORDER_ROUTING.getCode());
        Queue queue = new Queue(RabbitMqEnum.QueueName.SECONDARY_ORDER_DEAD_LETTER_QUEUE.getCode() ,true, false, false, args);
        rabbitAdmin.declareQueue(queue);
        log.info("queue {} create successed" ,RabbitMqEnum.QueueName.SECONDARY_ORDER_DEAD_LETTER_QUEUE.getCode());
        return queue;
    }

    /**
     * 订单
     * @param rabbitAdmin
     * @return
     */
    @Bean("orderQueue")
    Queue orderQueue(RabbitAdmin rabbitAdmin){
        Queue queue = new Queue(RabbitMqEnum.QueueName.SECONDARY_ORDER_QUEUE.getCode() ,true);
        rabbitAdmin.declareQueue(queue);
        log.info("queue {} create successed" ,RabbitMqEnum.QueueName.SECONDARY_ORDER_QUEUE.getCode());
        return queue;
    }

}
